FROM python:3.10.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


RUN addgroup --gid 1001 django && \
    useradd --uid 1001 --gid 1001 django


WORKDIR /code

COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt

COPY . /code/
EXPOSE 8000
CMD python manage.py migrate && python manage.py runserver 0.0.0.0:8000
